﻿using System.Drawing;

namespace Globals.Interfaces
{
    // Contract for all the edge detection operators
    public interface IOperator
    {
        byte[] FilterResult { get; set; }

        void CalculateRGBValues(Bitmap image, string typeOfEdgeFiltering, int dataStride, byte[] pixels);
        void SetOperatorResult(string type);
    }
}
