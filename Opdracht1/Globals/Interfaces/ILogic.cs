﻿using System.Drawing;

namespace Globals.Interfaces
{
    // Contract of Logic which is used by both algorithms
    public interface ILogic
    {
        Bitmap SelectFilter(Bitmap image, string typeOfEdgeFiltering, string algorithm);
        void SetToGrayscale(Bitmap image);
    }
}
