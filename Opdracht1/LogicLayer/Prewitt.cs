﻿using Globals.Interfaces;
using System;
using System.Drawing;

namespace LogicLayer
{
    // Class that contains the logic to set a prewitt filter to a bitmap image
    // https://epochabuse.com/csharp-sobel/
    public class Prewitt : IOperator
    {
        // Offset of center pixel from the border of the matrices
        private const int _filterValue = 1;

        public byte[] FilterResult { get; set; }

        // 3x3 convolution matrix for prewitt X (vertical)
        private static double[,] xPrewittMatrix
        {
            get
            {
                return new double[,]
                {
                    { 1, 0, -1 },
                    { 1, 0, -1 },
                    { 1, 0, -1 }
                };
            }
        }

        // 3x3 convolution matrix for prewitt Y (horizontal)
        private static double[,] yPrewittMatrix
        {
            get
            {
                return new double[,]
                {
                    {  1,  1,  1 },
                    {  0,  0,  0 },
                    { -1, -1, -1 }
                };
            }
        }

        // RGB X properties
        private double RX { get; set; }
        private double GX { get; set; }
        private double BX { get; set; }

        // RGB Y properties
        private double RY { get; set; }
        private double GY { get; set; }
        private double BY { get; set; }

        // RGB XY properties
        private double RXY { get; set; }
        private double GXY { get; set; }
        private double BXY { get; set; }

        private int ByteOffset { get; set; }
        private int CalculatedOffset { get; set; }


        // Method that provides the matrix calculations and sets the offset from the borders (top, left)
        public void CalculateRGBValues(Bitmap image, string typeOfEdgeFiltering, int dataStride, byte[] pixels)
        {
            for (int offsetY = _filterValue; offsetY < image.Height - _filterValue; offsetY++)
            {
                for (int offsetX = _filterValue; offsetX < image.Width - _filterValue; offsetX++)
                {
                    RY = GY = BY = BX = GX = RX = RXY = GXY = BXY = 0.0;

                    // Center pixel position of the matrix
                    ByteOffset = offsetY * dataStride + offsetX * 4;

                    // Matrix calculations
                    for (int prewittY = -_filterValue; prewittY <= _filterValue; prewittY++)
                    {
                        for (int prewittX = -_filterValue; prewittX <= _filterValue; prewittX++)
                        {
                            CalculatedOffset = ByteOffset + (prewittX * 4) + (prewittY * dataStride);
                            BX += (double)(pixels[CalculatedOffset]) * xPrewittMatrix[prewittY + _filterValue, prewittX + _filterValue];
                            GX += (double)(pixels[CalculatedOffset + 1]) * xPrewittMatrix[prewittY + _filterValue, prewittX + _filterValue];
                            RX += (double)(pixels[CalculatedOffset + 2]) * xPrewittMatrix[prewittY + _filterValue, prewittX + _filterValue];
                            BY += (double)(pixels[CalculatedOffset]) * yPrewittMatrix[prewittY + _filterValue, prewittX + _filterValue];
                            GY += (double)(pixels[CalculatedOffset + 1]) * yPrewittMatrix[prewittY + _filterValue, prewittX + _filterValue];
                            RY += (double)(pixels[CalculatedOffset + 2]) * yPrewittMatrix[prewittY + _filterValue, prewittX + _filterValue];
                        }
                    }

                    SetOperatorResult(typeOfEdgeFiltering);
                }
            }
        }

        // Limits the bytes between 0 and 255 and set new image data in byte array
        public void SetOperatorResult(string typeOfEdgeFiltering)
        {
            SetRGBXYValues(typeOfEdgeFiltering);

            if (RXY > 255) RXY = 255;
            else if (RXY < 0) RXY = 0;

            if (GXY > 255) GXY = 255;
            else if (GXY < 0) GXY = 0;

            if (BXY > 255) BXY = 255;
            else if (BXY < 0) BXY = 0;

            FilterResult[ByteOffset] = (byte)(BXY);
            FilterResult[ByteOffset + 1] = (byte)(GXY);
            FilterResult[ByteOffset + 2] = (byte)(RXY);
            FilterResult[ByteOffset + 3] = 255;
        }

        // Sets the total RGB values for a pixel
        private void SetRGBXYValues(string typeOfEdgeFiltering)
        {
            if (typeOfEdgeFiltering == "X")
            {
                RXY = Math.Sqrt((RX * RX));
                GXY = Math.Sqrt((GX * GX));
                BXY = Math.Sqrt((BX * BX));
            }
            else if (typeOfEdgeFiltering == "Y")
            {
                RXY = Math.Sqrt((RY * RY));
                GXY = Math.Sqrt((GY * GY));
                BXY = Math.Sqrt((BY * BY));
            }
            else if (typeOfEdgeFiltering == "XY")
            {
                RXY = Math.Sqrt((RY * RY) + (RX * RX));
                GXY = Math.Sqrt((GY * GY) + (GX * GX));
                BXY = Math.Sqrt((BY * BY) + (BX * BX));
            }
        }
    }
}
