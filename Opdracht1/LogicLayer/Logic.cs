﻿using Globals.Interfaces;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace LogicLayer
{
    public class Logic : ILogic
    {
        private IOperator sobel, prewitt;

        private Bitmap Result { get; set; }
        private BitmapData Data { get; set; }

        private float RGB { get; set; }
        private byte[] Pixels { get; set; }


        // Constructor
        public Logic(IOperator sobel, IOperator prewitt)
        {
            this.sobel = sobel;
            this.prewitt = prewitt;
        }


        // Creates the resulting bitmap of the selected filter 
        public Bitmap SelectFilter(Bitmap image, string typeOfEdgeFiltering, string algorithm)
        {
            SetImageBitmapData(image, algorithm);

            SetToGrayscale(image);

            if (algorithm == "sobel") sobel.CalculateRGBValues(image, typeOfEdgeFiltering, Data.Stride, Pixels);
            else if (algorithm == "prewitt") prewitt.CalculateRGBValues(image, typeOfEdgeFiltering, Data.Stride, Pixels);

            Result = new Bitmap(image.Width, image.Height);

            SetFilteredImageBitmapData(algorithm);

            return Result;
        }

        // Sets the bitmap image to grayscale
        // http://www.tannerhelland.com/3643/grayscale-image-algorithm-vb6/
        // https://epochabuse.com/csharp-grayscale/
        // https://epochabuse.com/csharp-sobel/
        public void SetToGrayscale(Bitmap image)
        {
            RGB = 0;

            // Luma formula
            for (int i = 0; i < Pixels.Length; i += 4)
            {
                RGB = Pixels[i] * 0.21f;
                RGB += Pixels[i + 1] * 0.71f;
                RGB += Pixels[i + 2] * 0.071f;

                Pixels[i] = (byte)RGB;
                Pixels[i + 1] = Pixels[i];
                Pixels[i + 2] = Pixels[i];
                Pixels[i + 3] = 255;
            }
        }


        // Buffer which reads pixel data of the original image
        private void SetImageBitmapData(Bitmap image, string algorithm)
        {
            Data = image.LockBits(new Rectangle(0, 0, image.Width, image.Height), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);

            if (algorithm == "sobel") sobel.FilterResult = new byte[Data.Stride * Data.Height];
            else if (algorithm == "prewitt") prewitt.FilterResult = new byte[Data.Stride * Data.Height];
            Pixels = new byte[Data.Stride * Data.Height];
           
            Marshal.Copy(Data.Scan0, Pixels, 0, Pixels.Length);
            image.UnlockBits(Data);
        }

        // Buffer which writes pixel data of the sobel or prewitt filter added image and copies it to the bitmap
        private void SetFilteredImageBitmapData(string algorithm)
        {
            Data = Result.LockBits(new Rectangle(0, 0, Result.Width, Result.Height), ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);

            if (algorithm == "sobel") Marshal.Copy(sobel.FilterResult, 0, Data.Scan0, sobel.FilterResult.Length);
            else if (algorithm == "prewitt") Marshal.Copy(prewitt.FilterResult, 0, Data.Scan0, prewitt.FilterResult.Length);

            Result.UnlockBits(Data);
        }
    }
}
