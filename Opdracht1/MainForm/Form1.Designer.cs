﻿namespace MainForm
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LoadBtn = new System.Windows.Forms.Button();
            this.sobelComboBox = new System.Windows.Forms.ComboBox();
            this.sobelPicBox = new System.Windows.Forms.PictureBox();
            this.prewittComboBox = new System.Windows.Forms.ComboBox();
            this.prewittPicBox = new System.Windows.Forms.PictureBox();
            this.originalPicBox = new System.Windows.Forms.PictureBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblSobel = new System.Windows.Forms.Label();
            this.lblPrewitt = new System.Windows.Forms.Label();
            this.lblSobelResult = new System.Windows.Forms.Label();
            this.lblPrewittResult = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.sobelPicBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prewittPicBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.originalPicBox)).BeginInit();
            this.SuspendLayout();
            // 
            // LoadBtn
            // 
            this.LoadBtn.Location = new System.Drawing.Point(12, 53);
            this.LoadBtn.Name = "LoadBtn";
            this.LoadBtn.Size = new System.Drawing.Size(75, 23);
            this.LoadBtn.TabIndex = 1;
            this.LoadBtn.Text = "Kies Foto";
            this.LoadBtn.UseVisualStyleBackColor = true;
            this.LoadBtn.Click += new System.EventHandler(this.LoadBtn_Click);
            // 
            // sobelComboBox
            // 
            this.sobelComboBox.Enabled = false;
            this.sobelComboBox.FormattingEnabled = true;
            this.sobelComboBox.Items.AddRange(new object[] {
            "Sobel X",
            "Sobel Y",
            "Sobel XY"});
            this.sobelComboBox.Location = new System.Drawing.Point(12, 135);
            this.sobelComboBox.Name = "sobelComboBox";
            this.sobelComboBox.Size = new System.Drawing.Size(262, 21);
            this.sobelComboBox.TabIndex = 3;
            this.sobelComboBox.SelectedIndexChanged += new System.EventHandler(this.sobelComboBox_SelectedIndexChanged);
            // 
            // sobelPicBox
            // 
            this.sobelPicBox.Location = new System.Drawing.Point(12, 452);
            this.sobelPicBox.Name = "sobelPicBox";
            this.sobelPicBox.Size = new System.Drawing.Size(800, 400);
            this.sobelPicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.sobelPicBox.TabIndex = 4;
            this.sobelPicBox.TabStop = false;
            // 
            // prewittComboBox
            // 
            this.prewittComboBox.Enabled = false;
            this.prewittComboBox.FormattingEnabled = true;
            this.prewittComboBox.Items.AddRange(new object[] {
            "Prewitt X",
            "Prewitt Y",
            "Prewitt XY"});
            this.prewittComboBox.Location = new System.Drawing.Point(12, 175);
            this.prewittComboBox.Name = "prewittComboBox";
            this.prewittComboBox.Size = new System.Drawing.Size(262, 21);
            this.prewittComboBox.TabIndex = 5;
            this.prewittComboBox.SelectedIndexChanged += new System.EventHandler(this.prewittComboBox_SelectedIndexChanged);
            // 
            // prewittPicBox
            // 
            this.prewittPicBox.Location = new System.Drawing.Point(834, 452);
            this.prewittPicBox.Name = "prewittPicBox";
            this.prewittPicBox.Size = new System.Drawing.Size(800, 400);
            this.prewittPicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.prewittPicBox.TabIndex = 6;
            this.prewittPicBox.TabStop = false;
            // 
            // originalPicBox
            // 
            this.originalPicBox.Location = new System.Drawing.Point(417, 10);
            this.originalPicBox.Name = "originalPicBox";
            this.originalPicBox.Size = new System.Drawing.Size(800, 400);
            this.originalPicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.originalPicBox.TabIndex = 7;
            this.originalPicBox.TabStop = false;
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(119, 53);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "Opslaan Resultaat";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblSobel
            // 
            this.lblSobel.AutoSize = true;
            this.lblSobel.Location = new System.Drawing.Point(9, 119);
            this.lblSobel.Name = "lblSobel";
            this.lblSobel.Size = new System.Drawing.Size(81, 13);
            this.lblSobel.TabIndex = 9;
            this.lblSobel.Text = "Sobel Operator:";
            // 
            // lblPrewitt
            // 
            this.lblPrewitt.AutoSize = true;
            this.lblPrewitt.Location = new System.Drawing.Point(9, 159);
            this.lblPrewitt.Name = "lblPrewitt";
            this.lblPrewitt.Size = new System.Drawing.Size(86, 13);
            this.lblPrewitt.TabIndex = 10;
            this.lblPrewitt.Text = "Prewitt Operator:";
            // 
            // lblSobelResult
            // 
            this.lblSobelResult.AutoSize = true;
            this.lblSobelResult.Location = new System.Drawing.Point(14, 427);
            this.lblSobelResult.Name = "lblSobelResult";
            this.lblSobelResult.Size = new System.Drawing.Size(85, 13);
            this.lblSobelResult.TabIndex = 11;
            this.lblSobelResult.Text = "Sobel Resultaat:";
            // 
            // lblPrewittResult
            // 
            this.lblPrewittResult.AutoSize = true;
            this.lblPrewittResult.Location = new System.Drawing.Point(831, 427);
            this.lblPrewittResult.Name = "lblPrewittResult";
            this.lblPrewittResult.Size = new System.Drawing.Size(90, 13);
            this.lblPrewittResult.TabIndex = 12;
            this.lblPrewittResult.Text = "Prewitt Resultaat:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1685, 864);
            this.Controls.Add(this.lblPrewittResult);
            this.Controls.Add(this.lblSobelResult);
            this.Controls.Add(this.lblPrewitt);
            this.Controls.Add(this.lblSobel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.originalPicBox);
            this.Controls.Add(this.prewittPicBox);
            this.Controls.Add(this.prewittComboBox);
            this.Controls.Add(this.sobelPicBox);
            this.Controls.Add(this.sobelComboBox);
            this.Controls.Add(this.LoadBtn);
            this.Name = "Form1";
            this.Text = "Sobel- en Prewittapplicatie";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.sobelPicBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prewittPicBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.originalPicBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button LoadBtn;
        private System.Windows.Forms.ComboBox sobelComboBox;
        private System.Windows.Forms.PictureBox sobelPicBox;
        private System.Windows.Forms.ComboBox prewittComboBox;
        private System.Windows.Forms.PictureBox prewittPicBox;
        private System.Windows.Forms.PictureBox originalPicBox;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblSobel;
        private System.Windows.Forms.Label lblPrewitt;
        private System.Windows.Forms.Label lblSobelResult;
        private System.Windows.Forms.Label lblPrewittResult;
    }
}

