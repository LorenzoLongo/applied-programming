﻿using Globals.Interfaces;
using LogicLayer;
using System;
using System.Windows.Forms;

namespace MainForm
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            IOperator sobel = new Sobel();
            IOperator prewitt = new Prewitt();
            ILogic logic = new Logic(sobel, prewitt);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1(logic));
        }
    }
}
