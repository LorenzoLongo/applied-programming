﻿#define myDebug

using Globals.Interfaces;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace MainForm
{
    // https://softwarebydefault.com/2013/05/11/image-edge-detection/
    public partial class Form1 : Form
    {
        private ILogic logic;

        private OpenFileDialog chooseFile;
        private Bitmap selectedImage, finalImage;

        // Constructor
        public Form1(ILogic logic)
        {
            this.logic = logic;
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            finalImage = new Bitmap(1, 1);
        }

        private void LoadBtn_Click(object sender, EventArgs e)
        {
            // Create new OpenFileDialog with properties
            chooseFile = new OpenFileDialog();
            chooseFile.Title = "Select a photo";

            if (chooseFile.ShowDialog() == DialogResult.OK)
            {
                selectedImage = new Bitmap(chooseFile.FileName);
                originalPicBox.Image = selectedImage;
            }

            sobelComboBox.Enabled = prewittComboBox.Enabled = true;
        }


        // Select Prewitt type of edge filter (horizontal, vertical or both)
        private void prewittComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            finalImage = new Bitmap(selectedImage.Width, selectedImage.Height);

            if (prewittComboBox.SelectedItem.ToString() == "Prewitt X") finalImage = logic.SelectFilter(selectedImage, "X", "prewitt");
            else if (prewittComboBox.SelectedItem.ToString() == "Prewitt Y") finalImage = logic.SelectFilter(selectedImage, "Y", "prewitt");
            else if (prewittComboBox.SelectedItem.ToString() == "Prewitt XY") finalImage = logic.SelectFilter(selectedImage, "XY", "prewitt");

            prewittPicBox.Image = finalImage;
            btnSave.Enabled = true;
        }

        // Select Sobel type of edge filter (horizontal, vertical or both)
        private void sobelComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            finalImage = new Bitmap(selectedImage.Width, selectedImage.Height);

            if (sobelComboBox.SelectedItem.ToString() == "Sobel X") finalImage = logic.SelectFilter(selectedImage, "X", "sobel");
            else if (sobelComboBox.SelectedItem.ToString() == "Sobel Y") finalImage = logic.SelectFilter(selectedImage, "Y", "sobel");
            else if (sobelComboBox.SelectedItem.ToString() == "Sobel XY") finalImage = logic.SelectFilter(selectedImage, "XY", "sobel");

            sobelPicBox.Image = finalImage;
            btnSave.Enabled = true;
        }

#if myDebug
        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.InitialDirectory = @"C:\Users\Loren\OneDrive\Bureaublad\Data\Data 1\clipart";
            dialog.DefaultExt = "jpg";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                finalImage.Save(dialog.FileName, ImageFormat.Jpeg);
            }
        }
#endif
    }
}
