﻿using Globals.Interfaces;
using System.Drawing;
using System.Linq;

namespace LogicLayer
{
    // Class which contains the logic to set the classifier attributes
    public class Logic : ILogic
    {
        private readonly IClassifier classifier;
        //private ImageStatistics rgbStats;

        // Attributes field
        private int[] attributes;
        private int[] histogramValues;

        // Constructor
        public Logic(IClassifier classifier)
        {
            this.classifier = classifier;
        }

        // Sets the attributes and returns if an image is a sunset or a nature picture
        public string ClassifyInputSource(Bitmap image)
        {
            attributes = new int[2];
            //rgbStats = new ImageStatistics(image);

            //attributes[0] = GetRedPeaks(rgbStats.Red.Values);
            //attributes[1] = GetBlueValues(rgbStats.Blue.Values);
            attributes[0] = GetRedPeaksCalculated(image);
            attributes[1] = GetBlueValuesCalculated(image);

            var result = classifier.ClassifyInputSource(attributes);

            if (result) return "sunset";

            return "nature";
        }

        // Method which gets the number of blue histogram values that are under a certain treshold (with library)
        private int GetBlueValues(int[] blueValues)
        {
            var counter = 0;

            for (int i = 0; i < blueValues.Length; i++)
            {
                if (blueValues[i] < 200) counter++;
            }

            return counter;
        }

        // Method which gets the number of blue histogram values that are under a certain treshold (without library)
        private int GetBlueValuesCalculated(Bitmap image)
        {
            var blueValues = GetHistogramValues(image, "blue");
            var counter = 0;

            for (int i = 0; i < blueValues.Length; i++)
            {
                if (blueValues[i] < 200) counter++;
            }

            return counter;
        }


        // Method which counts the number of peaks inside the red histogram (with library)
        private int GetRedPeaks(int[] redValues)
        {
            var treshold = redValues.Max() / 3;
            var peaks = 0;

            for (int i = 0; i < redValues.Length; i++)
            {
                if (redValues[i] > treshold) peaks++;
            }

            return peaks;
        }

        // Method which counts the number of peaks inside the red histogram (without library)
        private int GetRedPeaksCalculated(Bitmap image)
        {
            var redValues = GetHistogramValues(image, "red");
            var treshold = redValues.Max() / 3;
            var peaks = 0;

            for (int i = 0; i < redValues.Length; i++)
            {
                if (redValues[i] > treshold) peaks++;
            }

            return peaks;
        }


        // Sets the histogramvalues for the "Calculated" methods
        private int[] GetHistogramValues(Bitmap image, string color)
        {
            histogramValues = new int[256];

            for (int i = 0; i < image.Width; i++)
            {
                for (int j = 0; j < image.Height; j++)
                {
                    Color c = image.GetPixel(i, j);
                    long Temp = 0;

                    if (color == "red") Temp += c.R;
                    if (color == "blue") Temp += c.B;

                    histogramValues[Temp]++;
                }
            }
            return histogramValues;
        }
    }
}
