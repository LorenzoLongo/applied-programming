﻿using Globals.Interfaces;

namespace LogicLayer
{
    // Class which classifies pictures (sunset or nature)
    public class Classifier : IClassifier
    {
        // Method to check the total of blue values under the treshold and decide if it is a nature or sunset picture
        private bool CheckBlueValues(int[] attributes)
        {
            var boolean = false;

            if (attributes[1] <= 35) boolean = false;
            else if (attributes[1] > 35) boolean = true;

            return boolean;
        }

        // Method to check the total of red peaks and decide if it is a nature or sunset picture
        private bool CheckRedPeaks(int[] attributes)
        {
            var boolean = false;

            if (attributes[0] <= 85) boolean = CheckBlueValues(attributes);
            else if (attributes[0] > 85) boolean = false;

            return boolean;
        }

        // Method which returns if a picture is a nature or sunset
        public bool ClassifyInputSource(int[] attributes)
        {
            var classify = CheckRedPeaks(attributes);

            return classify;
        }
    }
}
