﻿namespace Opdracht2_AP
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picBoxImage = new System.Windows.Forms.PictureBox();
            this.lblSelectImage = new System.Windows.Forms.Label();
            this.lblTextResult = new System.Windows.Forms.Label();
            this.lblResult = new System.Windows.Forms.Label();
            this.btnSelectImage = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxImage)).BeginInit();
            this.SuspendLayout();
            // 
            // picBoxImage
            // 
            this.picBoxImage.Location = new System.Drawing.Point(172, 29);
            this.picBoxImage.Name = "picBoxImage";
            this.picBoxImage.Size = new System.Drawing.Size(400, 250);
            this.picBoxImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxImage.TabIndex = 0;
            this.picBoxImage.TabStop = false;
            // 
            // lblSelectImage
            // 
            this.lblSelectImage.AutoSize = true;
            this.lblSelectImage.Location = new System.Drawing.Point(12, 29);
            this.lblSelectImage.Name = "lblSelectImage";
            this.lblSelectImage.Size = new System.Drawing.Size(84, 13);
            this.lblSelectImage.TabIndex = 1;
            this.lblSelectImage.Text = "Select a picture:";
            // 
            // lblTextResult
            // 
            this.lblTextResult.AutoSize = true;
            this.lblTextResult.Location = new System.Drawing.Point(12, 241);
            this.lblTextResult.Name = "lblTextResult";
            this.lblTextResult.Size = new System.Drawing.Size(126, 13);
            this.lblTextResult.TabIndex = 2;
            this.lblTextResult.Text = "The selected picture is a:";
            // 
            // lblResult
            // 
            this.lblResult.AutoSize = true;
            this.lblResult.Location = new System.Drawing.Point(66, 266);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(16, 13);
            this.lblResult.TabIndex = 3;
            this.lblResult.Text = "...";
            // 
            // btnSelectImage
            // 
            this.btnSelectImage.Location = new System.Drawing.Point(15, 54);
            this.btnSelectImage.Name = "btnSelectImage";
            this.btnSelectImage.Size = new System.Drawing.Size(123, 23);
            this.btnSelectImage.TabIndex = 4;
            this.btnSelectImage.Text = "Select Picture";
            this.btnSelectImage.UseVisualStyleBackColor = true;
            this.btnSelectImage.Click += new System.EventHandler(this.btnSelectImage_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 303);
            this.Controls.Add(this.btnSelectImage);
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.lblTextResult);
            this.Controls.Add(this.lblSelectImage);
            this.Controls.Add(this.picBoxImage);
            this.Name = "Form1";
            this.Text = "Beslissingsboomprogramma";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picBoxImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picBoxImage;
        private System.Windows.Forms.Label lblSelectImage;
        private System.Windows.Forms.Label lblTextResult;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.Button btnSelectImage;
    }
}

