﻿using Globals.Interfaces;
using LogicLayer;
using System;
using System.Windows.Forms;

namespace Opdracht2_AP
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            IClassifier classifier = new Classifier();
            ILogic logic = new Logic(classifier);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1(logic));
        }
    }
}
