﻿using Globals.Interfaces;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Opdracht2_AP
{
    // Form class
    public partial class Form1 : Form
    {
        private readonly ILogic logic;

        private OpenFileDialog selectImage;
        private Bitmap image;

        // Constructor
        public Form1(ILogic logic)
        {
            this.logic = logic;
            InitializeComponent();
        }

        // Method to select an image and return if it is a sunset or nature picture
        private void btnSelectImage_Click(object sender, EventArgs e)
        {
            selectImage = new OpenFileDialog();
            selectImage.Title = "Select a photo";

            if (selectImage.ShowDialog() == DialogResult.OK)
            {
                image = new Bitmap(selectImage.FileName);
                picBoxImage.Image = image;
            }

            lblResult.Text = logic.ClassifyInputSource(image);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
