﻿using System.Drawing;

namespace Globals.Interfaces
{
    // Interface which contains the logic to set the attributes data
    public interface ILogic
    {
        string ClassifyInputSource(Bitmap image);
    }
}
