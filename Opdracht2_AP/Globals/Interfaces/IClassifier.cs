﻿namespace Globals.Interfaces
{
    // Interface for classifying
    public interface IClassifier
    {
        bool ClassifyInputSource(int[] attributes);
    }
}
